import React, { Component } from 'react';
import './ExerciseList.css';

import { Link } from "react-router-dom";

class ExerciseList extends Component {
  state = {
    exercises: ['pushups', 'squats']
  };
  render() {
    const renderList = this.state.exercises.map(exercise => {
      return <Link to={`/train/${exercise}`}>
        <div className="own-button">
          {exercise}
        </div>
      </Link>
    });

    return <div className="exercise-list">
      <p>Select your exercise</p>
      {renderList}
    </div>;
  }
}

export default ExerciseList;