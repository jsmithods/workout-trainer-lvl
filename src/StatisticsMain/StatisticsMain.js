import React, { Component } from 'react';
import './StatisticsMain.css';

import { Link } from "react-router-dom";

class StatisticsMain extends Component {
  state = {
    exercises: ['pushups', 'squats']
  };
  render() {
    const renderList = this.state.exercises.map(exercise => {
      return <Link to={`/statistics/${exercise}`}>
        <div className="own-button">
          {exercise}
        </div>
      </Link>
    });

    return <div className="exercise-list">
      <p>Select exercise for statistics</p>
      {renderList}
    </div>;
  }
}

export default StatisticsMain;