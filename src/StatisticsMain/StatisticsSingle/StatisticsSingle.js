import React, { Component } from 'react';
import './StatisticsSingle.css';

import {Bar} from 'react-chartjs-2';



class StatisticsSingle extends Component {
  state = {
    nav: {
      type: '12',
      id: 'udd'
    },
    exercise: {
      date: 0,
      work: [12,10]
    }
  };

  transformDate = (timestamp) => {
    const months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    const date = new Date(timestamp);
    const year = date.getFullYear();
    const month = months[date.getMonth()];
    const day = date.getDate();
    const hours = date.getHours();
    const minutes = date.getMinutes();
    const formattedDate = `${day}/${month}/${year}  ${hours}:${minutes}`;
    return formattedDate
  };

  sumElements = (total, num) => {
    return +total + +num;
  };

  componentWillMount() {
    const type = this.props.match.params.exercise;
    const id = this.props.match.params.id;
    let obj = {};
    let newNav = {type, id};
    this.setState({nav: newNav});
    const allExercises = JSON.parse(localStorage.getItem(type));
    const selected =  allExercises.filter(item => {
      return item.date == id;
    })[0];
    this.setState({exercise: selected});
  }

  render() {
    const chartData = {
      labels: [2, '3', '4', '5', '6', '7', '8'],
      datasets: [
        {
          label: 'Workput results',
          backgroundColor: 'rgba(255,99,132,0.2)',
          borderColor: 'rgba(255,99,132,1)',
          borderWidth: 1,
          hoverBackgroundColor: 'rgba(255,99,132,0.4)',
          hoverBorderColor: 'rgba(255,99,132,1)',
          data: this.state.exercise.work
        }
      ]
    };

    const sum = this.state.exercise.work.reduce(this.sumElements);
    const length = this.state.exercise.work.length;
    const avg = sum / length;
    return <div>
      {/*<p>{this.state.nav.type}</p>
      <p>{this.state.nav.id}</p>*/}

      <p>Date: {this.transformDate(this.state.exercise.date)}</p>
      <p>Sum: {sum}</p>
      <p>Avg: {avg}</p>

      <Bar
        data={chartData}
        width={100}
        height={50}
        options={{
          maintainAspectRatio: false
        }}
      />
    </div>;
  }
}

export default StatisticsSingle;