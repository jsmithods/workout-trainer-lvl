import React, { Component } from 'react';
import './StatisticsExercise.css';

import { Link } from "react-router-dom";

class StatisticsExercise extends Component {
  state = {
    exercises: [{
      date: 1543496335385,
      total: 120,
      avg: 26
    }, {
      date: 1543496335385,
      total: 100,
      avg: 10
    }]
  };

  transformDate = (timestamp) => {
    const months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    const date = new Date(timestamp);
    const year = date.getFullYear();
    const month = months[date.getMonth()];
    const day = date.getDate();
    const hours = date.getHours();
    const minutes = date.getMinutes();
    const formattedDate = `${day}/${month}/${year}  ${hours}:${minutes}`;
    return formattedDate
  };

  sumElements = (total, num) => {
    return +total + +num;
  };

  componentWillMount() {
    let currentRoutine = this.props.match.params.exercise;
    let allExercises = JSON.parse(localStorage.getItem(currentRoutine));
    let stateExercises = allExercises.map(item => {
      let obj = {};
      obj.date = item.date;
      obj.total = item.work.reduce(this.sumElements);
      obj.avg = obj.total / item.work.length;
      return obj;
    });
    this.setState({exercises: stateExercises});
    console.log(allExercises);
  }

  render() {
    const tableRows = this.state.exercises.map(item => {
      return <tr>
        <td>
          <Link to={`/statistics/${this.props.match.params.exercise}/${item.date}`}>
            {this.transformDate(item.date)}
          </Link>
        </td>
        <td>{item.total}</td>
        <td>{item.avg.toFixed(2)}</td>
      </tr>
    });
    const renderTable = <table>
      <tr>
        <th>Date</th>
        <th>Total</th>
        <th>Avg</th>
      </tr>
      {tableRows}
    </table>;
    return <div className="stats-table">
      <p>{this.props.match.params.exercise}</p>
      {renderTable}
    </div>;
  }
}

export default StatisticsExercise;