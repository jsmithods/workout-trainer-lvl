import React, { Component } from 'react';
import './DashboardTrackStatusBar.css';

import {config} from '../../../config/level-config';

class DashboardTrackStatusBar extends Component {
  countSum = (n) => {
    let first = config.first;
    let step = config.step;
    let sum = first;
    for (let i = 1; i < n; i++) {
      sum += first + i * step;
    }
    return sum;
  };

  countLevelAndScore = (total) => {
    let first = config.first;
    let step = config.step;
    let sum = first;
    let n = 1000;
    let lower = 0;
    let upper = 0;
    let current = 0;
    let target = 0;

    if (total < 100) {
      target = 100;
      lower = 0;
      upper = 1;
      current = total;
      // console.log({target, current, lower, upper});
      return {target, current, lower, upper};
    }

    for (let i = 1; i < n; i++) {
      sum += first + i * step;
      if (total < sum) {
        upper = i+1;
        lower = i;
        break;
      }
    }

    current = (total - this.countSum(lower));
    target = this.countSum(upper) - this.countSum(lower);
    // console.log({sum, lower, upper});
    return {target, current, lower, upper};
  };

  render() {
    const localTotal = this.props.total;
    const counted = this.countLevelAndScore(localTotal);

    const level = counted.lower;
    const breakScore = counted.target;
    const currentScore = counted.current;
    const percentage = ((currentScore / breakScore) * 100).toFixed(0);
    return <div className="progress-container">
      <div className="progress-level">
        <span>{level}</span>
      </div>
      <div className="progressbar">
        <div className="progressbar-status" style={{width: `${percentage}%`}}>{currentScore} / {breakScore}</div>
      </div>
    </div>;
  }
}

export default DashboardTrackStatusBar;