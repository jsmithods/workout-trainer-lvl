import React, { Component } from 'react';
import './DashboardTrack.css';

import DashboardTrackStatusBar from './DashboardTrackStatusBar/DashboardTrackStatusBar';

class DashboardTrack extends Component {
  render() {
    return <div className="dashboard-track">
      <div className="dashboard-track-info">
        <span className="dashboard-track-info-name">{this.props.name}</span>
        <span className="dashboard-track-info-total">{this.props.times}</span>
      </div>
      <DashboardTrackStatusBar total={this.props.times} />
    </div>;
  }
}

export default DashboardTrack;