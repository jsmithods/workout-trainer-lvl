import React, { Component } from 'react';
import './Dashboard.css';

import { Link } from "react-router-dom";

import DashboardTrack from './DashboardTrack/DashboardTrack';

class Dashboard extends Component {
  state = {
    tracks: [
      {
        name: "pushups",
        times: 50,
      }, {
        name: "squats",
        times: 400
      }
    ],
  };

  sumElements = (total, num) => {
    return +total + +num;
  };

  componentWillMount() {
    let oldState = this.state;
    let sumPushups = 0;
    let sumSquats = 0;

    // get pushups
    let allPushUps = JSON.parse(localStorage.getItem('pushups'));
    if (allPushUps !== null) {
      sumPushups = allPushUps.map(item => {
        return item.work.reduce(this.sumElements);
      }).reduce(this.sumElements);
    }

    // Get Squats
    let allSquats = JSON.parse(localStorage.getItem('squats'));
    if (allSquats !== null) {
      sumSquats = allSquats.map(item => {
        return item.work.reduce(this.sumElements);
      }).reduce(this.sumElements);
    }

    // Set pushups
    oldState.tracks[0].times = sumPushups;

    // Set squats
    oldState.tracks[1].times = sumSquats;
    this.setState({oldState});
  }

  render() {
    const tracks = this.state.tracks.map(track => {
      return  <DashboardTrack name={track.name} times={track.times} />
    });

    const startTraining = <Link to={'/list'}>
      <div className="own-button">
        start training
      </div>
    </Link>;

    const statisticsLink = <Link to={'/statistics'}>
      <div className="own-button">
        Show statistics
      </div>
    </Link>;

    return <div>
      {tracks}
      {startTraining}
      {statisticsLink}
    </div>;
  }
}

export default Dashboard;