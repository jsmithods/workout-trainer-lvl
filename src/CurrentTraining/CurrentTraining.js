import React, { Component } from 'react';
import './CurrentTraining.css';

class CurrentTraining extends Component {
  state = {
    total: 0,
    currentIterationNumber: 1,
    currentValue: 0,
    currentValues: [],
    isDone: false
  };

  handleCurrentInput = (event) => {
    this.setState({currentValue: event.target.value});
  };

  handleNextButton = () => {
    if (this.state.currentValue !== 0) {
      let oldTotal = this.state.total;
      let oldIterationNumber = this.state.currentIterationNumber;
      let oldCurrentValue = this.state.currentValue;
      this.setState({total: +(+oldTotal + +oldCurrentValue)});
      this.setState({currentIterationNumber: oldIterationNumber + 1});
      this.setState(prevState => ({
        currentValues: [...prevState.currentValues, oldCurrentValue]
      }));
      this.setState({currentValue: 0});
    }
  };

  handleDoneButton = () => {
    this.setState({isDone: true});
    let trainingType = this.props.match.params.exercise;
    let output = [];
    let savedTraining = {
      date: Date.now(),
      work: this.state.currentValues
    };
    let oldTrainings = JSON.parse(localStorage.getItem(trainingType));
    // console.log(oldTrainings);
    if (oldTrainings === null) {
      output.push(savedTraining);
    } else {
      oldTrainings.push(savedTraining);
      output = oldTrainings;
    }
    // console.log(output);

    localStorage.setItem(trainingType, JSON.stringify(output));
  };

  render() {
    const results = <div className="training-results">
      <h2>Your results</h2>
      <ul>
        <li>Iterations: {this.state.currentValues.length}</li>
        <li>Total done: {this.state.total}</li>
        <li>Values: {this.state.currentValues.map( item => <span> {item} </span>)}</li>
      </ul>
    </div>;

    const training = <div>
      <p className="training-title">CurrentTraining: {this.props.match.params.exercise}</p>
      <p>Current iteration {this.state.currentIterationNumber}</p>
      <p>Total Done: {this.state.total}</p>
      <div className="training-form">
        <input value={this.state.currentValue} onChange={this.handleCurrentInput} type="number"/>
      </div>
      <div className="training-button-container">
        <div className="training-button done-button" onClick={this.handleDoneButton}>Done</div>
        <div className="training-button next-button" onClick={this.handleNextButton}>Next</div>
      </div>
    </div>;

    const renderedContent = this.state.isDone ? results : training;
    return <div>
      {renderedContent}
    </div>;
  }
}

export default CurrentTraining;