import React, { Component } from 'react';
import './App.css';

import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import CurrentTraining from './CurrentTraining/CurrentTraining';
import Dashboard from './Dashboard/Dashboard';
import ExerciseList from './ExerciseList/ExerciseList';
import StatisticsMain from './StatisticsMain/StatisticsMain';
import StatisticsExercise from './StatisticsMain/StatisticsExercise/StatisticsExercise';
import StatisticsSingle from './StatisticsMain/StatisticsSingle/StatisticsSingle';

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Router>
          <div className="App">
            <Link to="/">Home</Link>
            <Route exact path="/" component={Dashboard} />
            <Route exact path={'/train/:exercise'} component={CurrentTraining} />
            <Route exact path="/list" component={ExerciseList} />
            <Route exact path="/statistics" component={StatisticsMain} />
            <Route exact path="/statistics/:exercise" component={StatisticsExercise} />
            <Route exact path="/statistics/:exercise/:id" component={StatisticsSingle} />
          </div>
        </Router>
      </React.Fragment>
    );
  }
}

export default App;
